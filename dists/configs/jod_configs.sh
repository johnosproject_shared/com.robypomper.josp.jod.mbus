#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

###############################################################################
# Usage:
# no direct usage, included from other scripts
#
# Example configs script called by JOD distribution scripts.
# This configuration can be used to customize the JOD distribution management
# like execution, installation, etc...
#
# Artifact: JOD Dist Template
# Version:  1.0.1
###############################################################################

# JOD_YML
# Absolute or $JOD_DIR relative file path for JOD config file, default $JOD_DIR/jod.yml
#export JOD_YML="jod_2.yml"

# JAVA_HOME
# Full path of JAVA's JVM (ex: $JAVA_HOME/bin/java)
#JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_251.jdk/Contents/Home"

# ################# #
# JOD MBus's config #
# ################# #

# ...
export JOD_MBUS_TYPE="ALL" # or "SLAVE"

# ...
export JOD_MBUS_DEV="/dev/ttyAMA0"

# ...
export JOD_MBUS_BAUD="2400"

# ##################### #
# JOD MBus-ALL's config #
# ##################### #

# ...
export JOD_MBUS_ADDR_TYPE="primary" # or 'secondary'

# ####################### #
# JOD MBus-SLAVE's config #
# ####################### #

# ...
#export JOD_MBUS_SLAVE_ADDR="394335734D6A8104"       #"94782644C5140004"

# ####################### #
# JOD MBus-ALL's examples #
# ####################### #

#export JOD_MBUS_TYPE="ALL"
#export JOD_MBUS_DEV="/dev/ttyAMA0"
#export JOD_MBUS_BAUD="2400"
#export JOD_MBUS_ADDR_TYPE="primary"                 # or 'secondary'

# ######################### #
# JOD MBus-SLAVE's examples #
# ######################### #

#export JOD_MBUS_TYPE="SLAVE"
#export JOD_MBUS_DEV="/dev/ttyAMA0"
#export JOD_MBUS_BAUD="2400"
#export JOD_MBUS_SLAVE_ADDR="394335734D6A8104"       #"94782644C5140004"
