# JOD MBus - 1.0

This JOD Distribution allow to startup and manage a JOD Agent that represent MBus devices. This object use the [libmbus](https://github.com/rscada/libmbus) libraries to communicate with the MBus, so it's compatible with all HW configurations
(MBus interface + Firmware) that can be used via that library.

## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD MBus @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_mbus/)
| **Repository**        | [com.robypomper.josp.jod.mbus @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.mbus/)
| **Downloads**            | [com.robypomper.josp.jod.mbus > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.mbus/downloads/)

This distribution allow to represent MBus slaves as JOD objects. It can scan slave and communicate with them via a serial port with the [libmbus](https://github.com/rscada/libmbus)
library.

Depending on distribution configs, it can scan all available slaves on configured MBus and represent all of them as a single JOD Object. Otherwise, it can be configured with a slave's address and then the distribution represent a single slave with a single JOD object.

For both cases the status exposed in the JOD objects depends on slave's type. On each boot, the JOD distribution will query data for each slave rapresented and convert each ```DataRecord``` in a JOD state.

In the ```configs/configs.sh``` file you can find and configure following properties:

| Property | Example | Description |
|----------|---------|-------------|
| **JOD_MBUS_TYPE** | ALL  | Select the distribution working mode: 'ALL' means the JOD object will contains all slave devices found on the MBus; 'SLAVE' means that the JOD object will contain only a single device from the MBus |
| **JOD_MBUS_DEV** | /dev/ttyAMA0  | The serial port's device where the MBus is connected to. |
| **JOD_MBUS_BAUD** | 2400  | The baud rate to use for the serial communication to the MBus. |
| **JOD_MBUS_ADDR_TYPE*** | philips-hue-tres  | It can be "primary" (then it scan primary addresses) otherwise it can be "secondary". This property is required only when ```JOD_MBUS_TYPE``` is 'ALL' |
| **JOD_MBUS_SLAVE_ADDR*** | 94782644C5140004 | Address to the MBus's slave to represent. This property is required only when ```JOD_MBUS_TYPE is``` 'SLAVE' |

_Properties with (*) are optionals depending on selected ```JOD_MBUS_TYPE``` mode._

## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server, object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.