#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Generate a new Object's structure config file representing configured MBus slave.
#
# Remove current struct.jod file, if any, then replace all placeholder from
# struct
# Fetch the Philips Hue Gateway until user press the physical button on the
# gateway. This script retry max for MAX_RETRY_COUNT times, after that prints
# an error message and exit the script execution.
#
# Artifact: JOD Philips Hue
# Version:  1.0
###############################################################################

JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/../.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"
source "$JOD_DIR/scripts/jod/struct/builder.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs

setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

###############################################################################
logScriptInit

# Standard configs
JOD_STRUCT=$JOD_DIR/configs/struct.jod

logInf "Generate John Object's structure for the MBus protocol..."

SLAVE_LIST_FILE="$JOD_DIR/tmp_list.txt"

# Script setup
export LD_LIBRARY_PATH=/usr/local/lib # required by mbus tools

if [[ "$JOD_MBUS_ADDR_TYPE" == "primary" ]]; then
  MBUS_SCAN_CMD="mbus-serial-scan"
  PARSING_SCAN_STR='s|Found a M-Bus device at address \(.*\)|\1|'
  SCAN_DEBUG="Found a M-Bus device at address 1
Found a M-Bus device at address 2
Found a M-Bus device at address 48"
elif [[ "$JOD_MBUS_ADDR_TYPE" == "secondary" ]]; then
  MBUS_SCAN_CMD="mbus-serial-scan-secondary"
  PARSING_SCAN_STR='s|Found a device on secondary address \(.*\) \[using address mask .*\]|\1|'
  SCAN_DEBUG="Found a device on secondary address 181129145F1D0102 [using address mask 1FFFFFFFFFFFFFFF]
Found a device on secondary address 3013731593060104 [using address mask 3FFFFFFFFFFFFFFF]
Found a device on secondary address 99999999FFFF0102 [using address mask 99FFFFFFFFFFFFFF]"
fi

# ################
# Get devices list
# ################

logInf "Querying MBus slaves list on '$JOD_MBUS_ADDR_TYPE' addresses (this can require some minutes)..."
logDeb "$MBUS_SCAN_CMD -b $JOD_MBUS_BAUD \"$JOD_MBUS_DEV\" > \"$SLAVE_LIST_FILE\""
date
$MBUS_SCAN_CMD -b $JOD_MBUS_BAUD "$JOD_MBUS_DEV" >"$SLAVE_LIST_FILE" || logFat "Error on querying MBus devices list (cmd $MBUS_SCAN_CMD -b $JOD_MBUS_BAUD \"$JOD_MBUS_DEV\" > \"$SLAVE_LIST_FILE\")"
date
#echo "$SCAN_DEBUG" > "$SLAVE_LIST_FILE"

SLAVE_LIST=()
while IFS='' read -r line; do SLAVE_LIST+=("$line"); done < <(sed "$PARSING_SCAN_STR" "$SLAVE_LIST_FILE")
[ "${#SLAVE_LIST[@]}" -eq 0 ] && logFat "No slaves found on MBus"
logInf "MBus slaves list queried successfully"

# #######################
# Create object structure
# #######################

logInf "Create object's structure..."

MODEL="MBus Gateway"
BRAND="John OS"
DESCR="The MBus gateway (serial-device: '$JOD_MBUS_DEV')"
DESCR_LONG="This object represent all salves connected to the MBus's gateway."
ROOT=$(buildComponent "Root" "$MODEL" "$BRAND" "$DESCR" "$DESCR_LONG")

# ##############
# For each slave

for JOD_MBUS_SLAVE_ADDR in "${SLAVE_LIST[@]}"; do
  logDeb "- Generate '$JOD_MBUS_SLAVE_ADDR' slave"

  # ###############
  # Get device data
  # ###############

  SLAVE_DATA_FILE="$JOD_DIR/tmp_{$JOD_MBUS_SLAVE_ADDR}_data.xml"
  logDeb "-- Querying slave device's data..."
  mbus-serial-request-data -b $JOD_MBUS_BAUD "$JOD_MBUS_DEV" "$JOD_MBUS_SLAVE_ADDR" >"$SLAVE_DATA_FILE" || logWar "Error on querying '$JOD_MBUS_SLAVE_ADDR' slave device's data, skipp slave"

  # #################
  # Parse device data
  # #################

  read_dom() {
    local IFS=\>
    read -d \< ENTITY CONTENT
    local RET=$?
    TAG_NAME=${ENTITY%% *}
    ATTRIBUTES=${ENTITY#* }
    return $RET
  }

  logDeb "-- Parsing slave device's data..."
  SENSORS=()
  COUNT=0
  while read_dom; do
    CONTENT="$(echo -e "$CONTENT" | tr -d '[\t\r\n]')"

    if [[ $ENTITY == ?xml* || $ENTITY == MBusData* || "$ENTITY" == "" ]]; then
      IGNORE=""

    elif [[ $TAG_NAME == /DataRecord* ]]; then
      #echo "SENSOR-END"
      COUNT=$((COUNT + 1))
      logDeb "--- added '$PARSING_SENSOR_ID/$PARSING_SENSOR_UNIT' sensor"
      #echo " > $PARSING_SENSOR_ID: $PARSING_SENSOR_FUNCTION, $PARSING_SENSOR_STORAGE_NUMBER, $PARSING_SENSOR_UNIT, $PARSING_SENSOR_VALUE, $PARSING_SENSOR_TIMESTAMP, $PARSING_SENSOR_DEVICE, $PARSING_SENSOR_TARIFF"
      SENSORS+=("$PARSING_SENSOR_ID,$PARSING_SENSOR_FUNCTION,$PARSING_SENSOR_STORAGE_NUMBER,$PARSING_SENSOR_UNIT,$PARSING_SENSOR_VALUE,$PARSING_SENSOR_TIMESTAMP,$PARSING_SENSOR_DEVICE,$PARSING_SENSOR_TARIFF")

    elif [[ $TAG_NAME == /* ]]; then
      IGNORE=""

    elif [[ $TAG_NAME == DataRecord* ]]; then
      #echo "SENSOR-START $CONTENT"
      #echo "Reset parsing sensor"
      PARSING_SENSOR_ID="${ATTRIBUTES#*\"}"
      PARSING_SENSOR_ID="${PARSING_SENSOR_ID%\"*}"
      PARSING_SENSOR_FUNCTION="N/A"
      PARSING_SENSOR_STORAGE_NUMBER="N/A"
      PARSING_SENSOR_UNIT="N/A"
      PARSING_SENSOR_VALUE="N/A"
      PARSING_SENSOR_TIMESTAMP="N/A"
      PARSING_SENSOR_DEVICE="-1"
      PARSING_SENSOR_TARIFF="-1"

    else
      case $TAG_NAME in
      # DataRecord tags
      Function) PARSING_SENSOR_FUNCTION=$CONTENT ;;
      StorageNumber) PARSING_SENSOR_STORAGE_NUMBER=$CONTENT ;;
      Unit) PARSING_SENSOR_UNIT=$CONTENT ;;
      Value) PARSING_SENSOR_VALUE=$CONTENT ;;
      Timestamp) PARSING_SENSOR_TIMESTAMP=$CONTENT ;;
      Device) PARSING_SENSOR_DEVICE=$CONTENT ;;
      Tariff) PARSING_SENSOR_TARIFF=$CONTENT ;;
      # SlaveInformation tags
      SlaveInformation) IGNORE="" ;;
      Id) DEVICE_ID=$CONTENT ;;
      Manufacturer) DEVICE_MANUFACTURER=$CONTENT ;;
      Version) DEVICE_VERSION=$CONTENT ;;
      ProductName) logDeb "--- Parsing '$CONTENT' device" && DEVICE_PRODUCT_NAME=$CONTENT ;;
      Medium) DEVICE_MEDIUM=$CONTENT ;;
      AccessNumber) DEVICE_ACCESS_NUMBER=$CONTENT ;;
      Status) DEVICE_STATUS=$CONTENT ;;
      Signature) DEVICE_SIGNATURE=$CONTENT ;;
      # Unknow tags
      *) logWar "--- Unknown tag '$TAG_NAME', ignored" ;;
      esac

    fi
  done <"$SLAVE_DATA_FILE"
  #rm "$SLAVE_DATA_FILE"

  ## Print device data
  #
  #echo "Device info:"
  #echo "Id:           $DEVICE_ID"
  #echo "Manufacturer: $DEVICE_MANUFACTURER"
  #echo "Version:      $DEVICE_VERSION"
  #echo "ProductName:  $DEVICE_PRODUCT_NAME"
  #echo "Medium:       $DEVICE_MEDIUM"
  #echo "AccessNumber: $DEVICE_ACCESS_NUMBER"
  #echo "Status:       $DEVICE_STATUS"
  #echo "Signature:    $DEVICE_SIGNATURE"
  #echo "SensorsCount: ${#SENSORS[@]}"
  #
  #echo "#   FUNCTION                      UNIT                                               VALUE                  S_N TIMESTAMP"
  #echo "--- ----------------------------- -------------------------------------------------- ---------------------- --- --------------------"
  #for SENSOR in "${SENSORS[@]}"; do
  #  IFS=',' read -r -a SENSOR_ARRAY <<< "$SENSOR"
  #  printf "%3s %-30s %-50s %20s | %s   %s\n" "${SENSOR_ARRAY[0]}" "${SENSOR_ARRAY[1]}" "${SENSOR_ARRAY[3]}" "${SENSOR_ARRAY[4]}" "${SENSOR_ARRAY[2]}" "${SENSOR_ARRAY[5]}";
  #done

  # Create slave container
  SLAVE=$(buildComponent "$DEVICE_PRODUCT_NAME ($DEVICE_ID)" "Container")

  for SENSOR in "${SENSORS[@]}"; do
    IFS=',' read -r -a SENSOR_ARRAY <<<"$SENSOR"

    PARSED_SENSOR_ID="${SENSOR_ARRAY[0]}"
    PARSED_SENSOR_FUNCTION="${SENSOR_ARRAY[1]}"
    PARSED_SENSOR_STORAGE_NUMBER="${SENSOR_ARRAY[2]}"
    PARSED_SENSOR_UNIT="${SENSOR_ARRAY[3]}"
    PARSED_SENSOR_VALUE="${SENSOR_ARRAY[4]}"
    PARSED_SENSOR_TIMESTAMP="${SENSOR_ARRAY[5]}"
    PARSED_SENSOR_DEVICE="${SENSOR_ARRAY[6]}"
    PARSED_SENSOR_TARIFF="${SENSOR_ARRAY[7]}"

    NAME="$PARSED_SENSOR_UNIT [$PARSED_SENSOR_ID]"
    DESCR="Sensor id: $PARSED_SENSOR_ID, unit: $PARSED_SENSOR_UNIT, function: $PARSED_SENSOR_FUNCTION, storageNumber: $PARSED_SENSOR_STORAGE_NUMBER, device: $PARSED_SENSOR_DEVICE, tariff: $PARSED_SENSOR_TARIFF"
    VALUE="$PARSED_SENSOR_VALUE"
    #printf "%-50s %-120s %20s\n" "$NAME" "$DESCR" "$VALUE"

    # Sensor's Components

    #logDeb "$MBUS_SCAN_CMD -b $JOD_MBUS_BAUD \"$JOD_MBUS_DEV\" > \"$SLAVE_LIST_FILE\""
    #PARSING_SCAN_STR='s|Found a device on secondary address \(.*\) \[using address mask .*\]|\1|'
    #while IFS='' read -r line; do SLAVE_LIST+=("$line"); done < <(sed "$PARSING_SCAN_STR" "$SLAVE_LIST_FILE")

    #cmd=osascript -e 'output volume of (get volume settings)';freq=1
    #"puller" : "shell://cmd=mbus-serial-request-data -b 2400 '/dev/ttyAMA0' 394335734D6A8104 | grep '<DataRecord id=\"0\">' -A 8 | grep '<Value>' | sed 's|"
    PULLER="shell://cmd=mbus-serial-request-data -b $JOD_MBUS_BAUD '$JOD_MBUS_DEV' $JOD_MBUS_SLAVE_ADDR | grep '<DataRecord id=\\\"$PARSED_SENSOR_ID\\\">' -A 8 | grep '<Value>' | sed 's|.*<Value>\\\(.*\\\)</Value>|\\\1|';freq=300"
    #PULLER="http://requestUrl='$STATION_URL';formatType=JSON;formatPath='$.data.$JOD_AA_ODH_STATION_TYPE.stations.$JOD_AA_ODH_STATION_CODE.sdatatypes.[\\\"$DATAPOINT_NAME\\\"].tmeasurements[0].mvalue';formatPathType=JSONPATH;"
    SENSOR=$(buildComponent "$NAME" "RangeState" "puller" "$PULLER")
    #SENSOR=$(buildComponent "$NAME" "RangeState" "puller" "$PULLER" 0 100 10)
    #18:23:30 - 18:24:00 scan 18:24:55 obj's struct 18:26:10 run java 18:26:50 jod start  94782644C5140004
    SLAVE=$(addSubComponent "$SLAVE" "$SENSOR")
  done

  ROOT=$(addSubComponent "$ROOT" "$SLAVE")
done
# For each slave
# ##############
logInf "Object's structure created successfully"

tryPrettyFormat "$ROOT" >"$JOD_STRUCT" || logWar "Error on parsing component ($ROOT)"

logInf "John Object's structure generated for the MBus gateway successfully"
